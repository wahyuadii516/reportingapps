import React, { Component } from 'react'
import { createStackNavigator, createAppContainer } from 'react-navigation'
import SplashScreen from '../Containers/SplashScreen'
import Login from '../Containers/Login'
import Register from '../Containers/Register'
import HomePage from '../Containers/HomePage'
import AddReport from '../Containers/HomePage/Home/AddReport'

const AppNavigator = createStackNavigator(
  {
      SplashScreen: { screen: SplashScreen },
      Login: { screen: Login },
      Register: { screen: Register },
      HomePage: { screen: HomePage },
      AddReport: { screen: AddReport },

  },
  {
    headerMode: 'none',
    initialRouteName: 'SplashScreen',
  },
)

export default createAppContainer(AppNavigator)
